package com.company.service;

import com.company.departments.BaseDepartment;
import com.company.interfaces.IDepartment;

public class Security extends BaseEmployee{

    public Security(String name, boolean free, IDepartment department) {
        super(name, free, department);
    }

    public Security(boolean free) {
        super(free);
    }

    public void openDoor(){

    }
    public void closeDoor(){

    }
    public void checkVisitor() {

    }

}


package com.company.goods;

import com.company.departments.BaseDepartment;
import com.company.interfaces.IDepartment;
import com.company.interfaces.IGood;

public abstract class BaseGoods implements IGood {
    private IDepartment Department;
    private String name;
    private boolean hasGuarantee;
    private String price;
    private String company;

    public BaseGoods(IDepartment department, String name, boolean hasGuarantee, String price, String company) {
        Department = department;
        this.name = name;
        this.hasGuarantee = hasGuarantee;
        this.price = price;
        this.company = company;
    }

    public BaseGoods(){}

    public BaseGoods(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getCompany() {
        return company;
    }

    public boolean isHasGuarantee() {
        return hasGuarantee;
    }

    public IDepartment getDepartment() {
        return Department;
    }
}


